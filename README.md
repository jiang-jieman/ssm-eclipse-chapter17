# ssm-eclipse-chapter17 

#### 介绍

教材 P253

基于SSM实现单个用户访问

#### 使用说明

- 注意修改 db.properties ,配置数据库连接信息。缺省使用远程数据库

- 注意配置文件功能分配

#### 快照

![输入图片说明](https://images.gitee.com/uploads/images/2020/1122/224538_b1eccd9d_382074.png "屏幕截图.png")

#### 参考论文评阅标准

![输入图片说明](https://images.gitee.com/uploads/images/2021/0512/064943_55de919c_382074.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0512/065001_77e1442f_382074.png "屏幕截图.png")
