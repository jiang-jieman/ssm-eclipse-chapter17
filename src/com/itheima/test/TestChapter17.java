package com.itheima.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.dao.CustomerDao;
import com.itheima.po.Customer;


public class TestChapter17 {

	@Test
	public void findCustomerByIdMapperTest(){	
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    CustomerDao customerDao = act.getBean(CustomerDao.class);   
	    Customer customer = customerDao.findCustomerById(1);
	    System.out.println(customer);
	}
}
