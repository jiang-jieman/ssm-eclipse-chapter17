<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客户信息</title>
<link rel="stylesheet" type="text/css"
	href="https://www.layuicdn.com/layui-v2.5.6/css/layui.css" />

</head>
<body>

	<table class="layui-table">
		<colgroup>
			<col width="150">
			<col width="150">
			<col width="150">
			<col width="150">
			<col>
		</colgroup>
		<thead>
			<tr>
				<th>编号</th>
				<th>名称</th>
				<th>职业</th>
				<th>电话</th>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>${customer.id}</td>
				<td>${customer.username}</td>
				<td>${customer.jobs}</td>
				<td>${customer.phone}</td>
			</tr>
		</tbody>
	</table>
	<script src="https://www.layuicdn.com/layui-v2.5.6/layui.js"></script>

</body>
</html>
